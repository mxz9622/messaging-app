//
//  CustomMessageCell.swift
//  MessagingApp
//
//  Created by Marko Živko on 02/07/2019.
//  Copyright © 2019 Marko Živko. All rights reserved.
//

import Foundation
import UIKit


class CustomMessageCell: UITableViewCell{

    //MARK: Part5c
    @IBOutlet var imageViewMessage: UIImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var bodyText: UILabel!
    @IBOutlet var messageBackground: UIView!
    
    
    
    
}
