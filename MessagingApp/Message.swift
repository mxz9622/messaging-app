//
//  Message.swift
//  MessagingApp
//
//  Created by Marko Živko on 02/07/2019.
//  Copyright © 2019 Marko Živko. All rights reserved.
//

import Foundation

class Message{
    
    var sender: String = ""
    var messageBody:String = ""
    
}
