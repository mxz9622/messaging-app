//
//  LoginViewController.swift
//  MessagingApp
//
//  Created by Marko Živko on 28/06/2019.
//  Copyright © 2019 Marko Živko. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD


class LoginViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Prijava"
    }
    
    //MARK: Part4
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        SVProgressHUD.show()
        
        if let email = emailTextField.text,
            let pass = passwordTextField.text{
            
            Auth.auth().signIn(withEmail: email, password: pass) { (user, error) in
                
                if error != nil{
                    print("Error while logging out")
                    SVProgressHUD.dismiss()
                }else{
                    print("login successful")
                    
                    SVProgressHUD.dismiss()
                    self.performSegue(withIdentifier: "goToChat", sender: self)
                }
                
            }
            
            //now we can go back to chat VC and manage tableViewDelegate

            
        }
        
    }

}
