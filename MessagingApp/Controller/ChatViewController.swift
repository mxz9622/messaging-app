//
//  ChatViewController.swift
//  MessagingApp
//
//  Created by Marko Živko on 28/06/2019.
//  Copyright © 2019 Marko Živko. All rights reserved.
//

import UIKit
import Firebase
import ChameleonFramework

//MARK: Part5a

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var messageTableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet var height: NSLayoutConstraint!
    
    var messageArray: [Message] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        title = "Razgovor :)"
        
        //MARK: Part3a
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Log out", style: .plain, target: self, action: #selector(addTapped))
        
        //MARK: Part5b
        messageTableView.delegate = self
        messageTableView.dataSource = self
        
        //MARK: Part6
        messageTextField.delegate = self
        
        //MARK: Part6bb
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tableViewTapped))
        messageTableView.addGestureRecognizer(tapGesture)
        
        //MARK: Part5e
        messageTableView.register(UINib(nibName: "MessageCell", bundle: nil), forCellReuseIdentifier: "customMessageCell")
        
        //styling - last touch final thing to do
        messageTableView.separatorStyle = .none
        
        configureTableView()
        retreiveMessages()
        
        
    }
    
    func configureTableView(){
        
        messageTableView.rowHeight = UITableView.automaticDimension
        messageTableView.estimatedRowHeight = 120.0
        
    }
    
    //MARK: Part6c
    @objc func tableViewTapped(){
        
        messageTextField.endEditing(true)
        
    }
    

    //MARK: Part3b

    @objc func addTapped(){
        
        print("logged out")
        
        //can throw an error
        do{
            //this is the line that can potentially cause an error
            try Auth.auth().signOut()
            
            //go back to our main viewController
            navigationController?.popToRootViewController(animated: true)

        }catch{
           print( error.localizedDescription)
        }
        
        //now we will manage login of our users
        
    }
    
    //MARK: part7
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        
        messageTextField.endEditing(true)
        
        //we dont want to jam our sender
        messageTextField.isEnabled = false
        sendButton.isEnabled = false
        
        //we need to create messaged DB inside firebase DB
        let messagesDB = Database.database().reference().child("Messages")
        
        let messageDictionary = ["Sender": Auth.auth().currentUser?.email, "Message Body": messageTextField.text!]
        
        //saving message dictionary inside messageDB with autoID
        messagesDB.childByAutoId().setValue(messageDictionary){
            
            (error, reference) in
            
            if error != nil{
                print(error!)
            }else{
                print("message saved!")
                
                //when message is saved and sent we can enable UI
                self.messageTextField.isEnabled = true
                self.sendButton.isEnabled = true
                self.messageTextField.text = ""
                
            }
            
            //now go and test sending the message and check your firebase db how it is being updated
        }
        
    }
    
    //MARK: part8
    func retreiveMessages(){
        
        let messageDB = Database.database().reference().child("Messages")
        
        //this closure will get called whenever new item is added to our firebase database
        messageDB.observe(.childAdded) { (DataSnapshot) in
            
            let snapshotValue = DataSnapshot.value as! Dictionary<String, String>
            
            let text = snapshotValue["Message Body"]!
            let sender = snapshotValue["Sender"]!
            
            //print(text, sender)
            
            //now set values in the UITableView
            
            let message = Message()
            
            message.messageBody = text
            message.sender = sender
            
            self.messageArray.append(message)
            self.configureTableView()
            self.messageTableView.reloadData()
            
            //and now we can set the tableUI to display our messages

            
        }
        
    }
    
    
    //before adopting to protocols, create custom message cell
    
    //MARK: Part5d
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "customMessageCell", for: indexPath) as! CustomMessageCell
        
        //check if it works with huge string
        //let messageArray = ["First", "Second", "Third"]
        
        //cell.bodyText.text = messageArray[indexPath.row]
        
        //MARK: part8a

        //now we will create real cell wiht real data
        cell.bodyText.text = messageArray[indexPath.row].messageBody
        cell.userName.text = messageArray[indexPath.row].sender
        
        //messages that we send
        if cell.userName.text == Auth.auth().currentUser?.email{
            
            cell.imageViewMessage.backgroundColor = UIColor.flatMint()
            cell.messageBackground.backgroundColor = UIColor.flatSkyBlue()
            
        }else{
            
            cell.imageViewMessage.backgroundColor = UIColor.flatWatermelon()
            cell.messageBackground.backgroundColor = UIColor.flatGray()
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //return 3

        //now we can go and create Message struct

        //MARK: part8b
        return messageArray.count
        //now we go to appDelegate which is currenlty deleting our database as soon as we run the app
        
    }

    //MARK: Part6a
    //called automatically when clicked on text field
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: 0.3) {
            
            self.height.constant = 318
            self.view.layoutIfNeeded()
            
        }
        
    }
    
    //does not get called automatically
    //we need custom tap gesture recognizer
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        UIView.animate(withDuration: 0.3) {
            
            self.height.constant = 50
            self.view.layoutIfNeeded()
            
        }
        
    }
    
    

    
    

}
