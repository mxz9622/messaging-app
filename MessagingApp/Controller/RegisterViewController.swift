//
//  RegisterViewController.swift
//  MessagingApp
//
//  Created by Marko Živko on 28/06/2019.
//  Copyright © 2019 Marko Živko. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class RegisterViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Registracija"

        // Do any additional setup after loading the view.
    }
    
    //MARK: Part2
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        
        //setup a new user
        
        //this is a closure - call back method
        //gets triggered once process is completed
        //you can give brief explanation on how they work and demo a small example
        
        //this prevents our app to get stuck and frozen
        //doing work in the background
        
        SVProgressHUD.show()
        
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            
            if error != nil{
                print(error!)
                SVProgressHUD.dismiss()
            }else{
                //success
                print("registration successful ")
                
                SVProgressHUD.dismiss()
                //now if the registration is a success we want to transfer our user to the app page
                self.performSegue(withIdentifier: "goToChat", sender: self)

            }
           
        }
        
        //now we can go to chat vc and create log out method
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
